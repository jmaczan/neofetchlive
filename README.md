# `neofetchlive`

[<img src="neofetch.png" height="240">](neofetch)

Run `neofetch` every N seconds (default 10)

## tl;dr
- Runs in terminal
- Requires `neofetch`
- It's a `bash` script
- Run with argument `./neofetchlive 5` to update each 5 seconds or without it `./neofetchlive` to run each 10 seconds

## Usage
- Install `neofetch`
- Download `neofetchlive`
- Make it executable `chmod +x neofetchlive`
- Run `./neofetchlive` in folder with script
- Optionally you can add path to `neofetchlive` to the `$PATH` variable in order to run it from anywhere in the system
- Exit using `Ctrl+C` keys combination

Credits to [Dylan](https://github.com/dylanaraps) and [contributors](https://github.com/dylanaraps/neofetch/graphs/contributors) for creating [`neofetch`](https://github.com/dylanaraps/neofetch).
